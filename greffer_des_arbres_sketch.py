#!/usr/bin/env python
# -*- coding: <utf-8> -*-

'''
It automatises the Oulipo constraint Litterature Definitionelle:
http://oulipo.net/fr/contraintes/litterature-definitionnelle

invented by Marcel Benabou in 1966
In a given phrase, one replaces every significant element (noun, adjective, verb, adverb) 
by one of its definitions in a given dictionary ; one reiterates the operation on the newly received phrase, 
and again.


Copyright (C) 2020 Constant, Algolit, Anaïs Berck, Gijs de Heij, Alice Neron

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details: <http://www.gnu.org/licenses/>.

'''


import fr_core_news_sm
import pickle
import random
import time
from arbres import arbres
import wikipedia
import os
import os.path
from colored import fg, attr
import re


## Text attributes
bold = attr(1)
underlined = (4)
reset = attr(0)

## Text colors
white = fg(15)
spring_green = fg(48)
light_gray = fg(7)
sky_blue = fg(109)
yellow = fg(3)

# get pickle with dictionary as set
dictionnaire = pickle.load(open("definitions_3rounds.obj", "rb"))
dictionnaire = list(dictionnaire)

# choose quote
def choix_arbre(arbres):
	collection = random.choice(list(arbres.items()))
	#print("collection:", collection)
	jardinier = collection[0]
	#print("jardinier:", jardinier)
	arbre = collection[1]
	#print("arbre:", arbre)
	return jardinier, arbre

# find bio gardener on Wikipedia
# def bio(jardinier):
# 	wikipedia.set_lang("fr")
# 	try:
# 		bio = wikipedia.page(jardinier)
# 		short_bio = bio.summary
# 	except:
# 		short_bio = "{}Il n'y a pas de page Wikipédia en français sur cette personne.{}".format(sky_blue, reset)
# 	return short_bio

def bio_offline(jardinier):
	filename = re.sub(r'\W', '', jardinier)
	source = "bios_fr/"+filename+".txt"
	if source:
		with open(source, 'r') as h:
			return h.read()
  
	return "{}Il n'y a pas de page Wikipédia en français sur cette personne.{}".format(sky_blue, reset)


# find nouns in quote
def trouver_les_bourgeons(arbre):
	# set-up Spacy for POS tagging
	nlp = fr_core_news_sm.load()
	doc = nlp(arbre)
	# find buds/nouns an their positions in quote
	bourgeons = []
	for bourgeon in doc :
		tag = f'{bourgeon.text} < {bourgeon.pos_}'
		if "NOUN" in tag:
			bourgeon_fertile = bourgeon
			bourgeons.append(bourgeon_fertile)
	return bourgeons

# find definitions for nouns from dictionary
def greffer(bourgeons):
	greffes = []
	branche = ""
	for bourgeon in bourgeons:
		bourgeon = bourgeon.text
		greffe = [word for word in dictionnaire if word[0] == bourgeon]
		if greffe:
			greffe = greffe[0]
			#print("greffe:", greffe)
			greffes.append(greffe)
	return greffes

# replace nouns in quote by their definitions
def greffes_qui_poussent(greffes, arbre):
	greffe = random.choice(greffes)
	mot = greffe[0]
	# print("mot:", mot)
	if mot in arbre:
		definition = greffe[1]
		definition = definition.lower()
		definition = definition.strip()
		position = arbre.find(mot)

		# print("definition:", definition)
		# arbre_greffe = arbre.replace(mot, definition)
	return (arbre[:position], mot, definition, arbre[position+len(mot):])

def show (state, length=1):
  os.system('clear')
  # Optionally vertically align here
  print(state)
  time.sleep(length)

# EXECUTE
# find quote & author
jardinier, arbre = choix_arbre(arbres)
# print("jardinier:", jardinier, "arbre:", arbre)
# find bio author
short_bio = bio_offline(jardinier)
# print("bref bio:", short_bio)

show("L'arbre initial est une citation de {}{}{}\n\n".format(bold, jardinier, reset) + short_bio, 4)


while len(arbre) < 1500:
	# find nouns and their positions in quote
	bourgeons = trouver_les_bourgeons(arbre)
	# print("bourgeons:", bourgeons)

	# find definitions for nouns from dictionary
	greffes = greffer(bourgeons)
	# print("greffes:", greffes)

	# replace nouns in quote by their definitions
	head, mot, definition, tail = greffes_qui_poussent(greffes, arbre)


	# Make new tree placeholder for the bud show we can show it in two states and then grow out
	arbre_greffe = '{} {{}} {}'.format(head, tail)

	show(arbre, 3)
	# Mark the bud, underlined and orange
	show(arbre_greffe.format("{}{}{}{}{}".format(bold, yellow, mot, reset, reset)), 3)
	# Mark the new branch green
	show(arbre_greffe.format("{}{}{}".format(spring_green, definition, reset)), 8)
	# show(arbre_greffe.format("{}{}{}".format(fg(82), definition, reset)), 1)
	# show(arbre_greffe.format("{}{}{}".format(fg(120), definition, reset)), 1)
	# show(arbre_greffe.format("{}{}{}".format(fg(157), definition, reset)), 1)
	# show(arbre_greffe.format("{}{}{}".format(fg(195), definition, reset)), 1)


	# convert changed quote into new tree to be grafted
	arbre = arbre_greffe.format(definition)


