from arbres import arbres
import wikipedia
import re
import os.path
import time



# find bio gardener on Wikipedia
def bio(jardinier):
	wikipedia.set_lang("fr")
	try:
		text = wikipedia.page(jardinier)
		short_bio = text.summary
	except:
		short_bio = "Il n'y a pas de page Wikipédia en français sur cette personne."
	return short_bio

for jardinier in arbres.keys():
	jardinier = jardinier.strip()
	if jardinier:
		print('Downloading bio for {}'.format(jardinier))
		text = bio(jardinier)
		print("bio:", text)
		filename = re.sub(r'\W', '', jardinier)
		print("filename:", filename)
		
		destination = "bios_fr/"+filename+".txt"	
		with open(destination, 'w') as h:
			h.write(text)


	time.sleep(.5)