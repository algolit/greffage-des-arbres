#!/usr/bin/env/ python


arbres = {"David Prescott": "La vision d'un arbre touche forcément l'âme de tout un chacun.",\
"Novalis":"Les fleurs, les arbres, tout poussait avec force, tout verdissait avec vigueur. \
Il semblait que tout avait reçu une âme. Tout parlait tout chantait.",\
"Victor Hugo": "C'est que l'amour est comme un arbre, il pousse de lui-même, jette profondément ses racines \
dans tout notre être, et continue souvent de verdoyer sur un cœur en ruine.",\
"Jacques Prévert": "Autrefois les bûcherons avaient des égards pour les arbres autrefois les bûcherons \
buvaient à leur santé.",\
"René Hausman": "Lorsque, la hache à la main, le bûcheron atteint la forêt, les arbres se disent : \
le manche est des nôtres...",\
"Rocé": "Les humains sont comme des arbres, ils ont des racines aux semelles.",\
"Nicholas Evans": "Il aura fallu des millions d'années à l'espèce humaine pour descendre des arbres et \
seulement dix de plus pour se mettre en vitrine.",\
"Richard Powers": "Il n'y a proportionnellement pas assez de forêts pour le nombre de mauvais écrivains qui peuplent ce pays.",\
"Sitting Bull": "Quand ils auront coupé le dernier arbre, pollué le dernier ruisseau, pêché le dernier poisson.\
 Alors ils s’apercevront que l’argent ne se mange pas.",\
"Denis Langlois": "La géométrie est importante pour les arbres. Passer de la verticale à l'horizontale leur\
 est généralement fatal.", 
"Denis Langlois": "En période de canicule, ce sont les arbres qui sont les plus à plaindre. Ils ne peuvent pas se mettre à l'ombre.",\
"Claude Ponti": "Depuis qu'il sait qu'on fait des livres avec le bois des arbres, O'Messi-Messian rêve au \
livre qu'il deviendra plus tard, quand sa vie d'arbre sera finie.",\
"Kama Sywor Kamanda": "Quand les branches des arbres dansent aux vents, les esprits, en cet instant magique,\
 voyagent en quête d'incantation.",\
"André Breton": "Campagne d'os blancs et rouges, qu'as-tu fait de tes arbres immondes, de ta candeur arborescente,\
 de ta fidélité qui était une bourse aux perles serrées, avec des fleurs, des inscriptions comme ci comme ça, \
 des significations à tout prendre ?",\
"Sylvain Tesson": "Les arbres nous enseignent une forme de pudeur et de savoir-vivre. Ils poussent vers la \
lumière en prenant soin de s'éviter, de ne pas se toucher, et leurs frondaisons se découpent dans le ciel \
sans jamais pénétrer dans la frondaison voisine. Les arbres, en somme, sont très bien élevés, \
ils tiennent leurs distances. Ils sont généreux aussi.", \
"Sylvain Tesson": "La forêt est un organisme total, composé de milliers d'individus. Chacun est appelé à naître, à vivre, à mourir, à se décomposer - \
à assurer aux générations suivantes un terreau de croissance supérieur à celui sur lequel il avait poussé. \
Chaque arbre reçoit et transmet. Entre les deux, il se maintient. La forêt ressemble à ce que devrait être \
une culture.",\
"Pierre Rabhi": "Pour que les arbres et les plantes s'épanouissent, pour que les animaux qui s'en nourrissent \
prospèrent, pour que les hommes vivent, il faut que la terre soit honorée.",\
"Anaïs Nin": "Le monde d'aujourd'hui a perdu ses racines. C'est une grande forêt où les arbres seraient \
plantés la tête en bas. Leurs racines gesticulent furieusement en l'air et elles se dessèchent.",\
"Didier Comès": "Tu es celui pour qui les arbres bourgeonnent et les fleurs s'émeuvent !",\
"Erri De Luca": "Les arbres de montagne écrivent dans l'air des histoires qui se lisent quand on est allongé dessous.",\
"William Blake": "Un sot ne voit pas le même arbre qu'un sage.",\
"Henri Duvernois": "Et dire que les vieux arbres sont si beaux ! Hélas, on n'est pas de bois.",\
"Belur Krishnamacharya Sundararaja Iyengar": "Les sages d'autrefois comparaient le yoga à un arbre fruitier.\
 D'une seule graine naissent les racines, le tronc, les branches et les feuilles.",\
"Haruki": "Moi, j'adore regarder les arbres. Depuis toute petite, et même maintenant. \
Quand j'ai le temps, je m'assieds sous un arbre, je touche le tronc, ou bien je lève les yeux vers les branches,\
 je peux rester des heures comme ça sans rien faire de plus.",\
"Christian Bobin":"Le monde entier repose sur nous. Il dépend d'un grain de silence, d'une poussière d'or- \
de la ferveur de notre attente. Un arbre éblouissant de vert. Un visage inondé de lumière. \
Cela suffit bien chaque jour. C'est même beaucoup. Voir ce qui est. Être ce qu'on voit. \
S'égarer dans les livres, ou dans les bois.",\
"Jean Bertaut": "Se plaindre de sentir des ennuis et des peines, C'est se plaindre d'être un homme et non arbre \
ou rocher.",\
"Lee Seung-U": "L'aliboufier, un arbre voluptueux, svelte et souple comme un corps de femme. \
Il enlaçait le pin dans une tendre étreinte. J'imagine que, sous terre, leurs racines s'entremêlaient \
dans une intimité encore plus scandaleuse.",\
"Léon Bloy": "Par nature, le Bourgeois est haïsseur et destructeur de paradis. Quand il aperçoit un beau Domaine,\
 son rêve est de couper les grands arbres, de tarir les sources, de tracer des rues, d'instaurer des boutiques \
 et des urinoirs. Il appelle ça monter une affaire.",\
"Wangari Maathai": "Plantons des arbres et les racines de notre avenir s’enfonceront dans le sol et une canopée \
de l’espoir s’élèvera vers le ciel.",\
"Julien Gracq": "Dans le silence des arbres, à peine distinct de celui des étoiles, ils vécurent une nuit du \
monde dans sa privauté sidérale, et la révolution de la planète, son orbe enthousiasmante parut gouverner \
l'harmonie de leurs gestes les plus familiers.",\
"Emil Cioran": "Des arbres massacrés. Des maisons surgissent. Des gueules, des gueules partout. \
L'homme s'étend. L'homme est le cancer de la terre.",\
"Honoré de Balzac": "Il ne se rencontre pas plus dans la vie de l'homme deux moments de plaisir semblables, \
qu'il n'y a deux feuilles exactement pareilles sur un même arbre.",\
"Rainer Maria Rilke": "Être artiste veut dire ne pas calculer, ne pas compter, mûrir tel un arbre qui ne presse\
 pas sa sève, et qui, confiant, se dresse dans les tempêtes printanières sans craindre que l'été puisse \
 ne pas venir.",\
"Jean-Marie Gourio": "C'est pas la nature qui décide du nombre des arbres. C'est la mairie.",\
"Victor Hugo": "On ne tourmente pas les arbres stériles et desséchés ; ceux-là seulement sont battus de \
pierres dont le front est couronné de fruits d'or.",\
"Fernando Pessoa": "Il n'est pas suffisant de ne pas être aveugle pour voir les arbres et les fleurs. \
Il faut aussi n'avoir aucune philosophie. Quand il y a philosophie, il n'y a pas d'arbres : \
il y a des idées, sans plus.",\
"Schopenhauer": "Le chien est, à juste titre, le symbole de la fidélité ; parmi les plantes, ce devrait être \
le sapin. Lui seul, en effet, tient bon avec nous, que la saison soit belle ou mauvaise, et ne nous abandonne \
pas en même temps que le soleil nous retire sa faveur, comme font tous les autres arbres, plantes, insectes \
et oiseaux, pour reparaître quand le ciel nous rit de nouveau.",\
"Jacques de Bourbon Busset": "La forêt se dérobe à la lumière et c’est ainsi qu’elle dure. Certes elle assimile \
les richesses du soleil, mais les transforme, les élabore, les conserve. Il y a une grande force dans ce retrait,\
 dans ce recueillement. Je voudrais être un arbre, un arbre qui marche.",\
"Jules Renard": "Comprends la vie mieux que moi, moins petitement, et garde toujours ta pensée à la hauteur \
des arbres.",\
"René Char": "Les arbres ne se questionnent pas entre eux, mais trop rapprochés, ils font le geste de s'éviter."}

