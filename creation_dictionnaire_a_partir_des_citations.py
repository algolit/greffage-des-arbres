# -*- coding: utf-8 -*-

from arbres import arbres
import requests
from bs4 import BeautifulSoup
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import fr_core_news_sm
import re
import pickle



# function to find nouns
def find_nouns(citation):
	# set-up Spacy for POS tagging
	nlp = fr_core_news_sm.load()
	doc = nlp(citation)

	# find nouns in quote
	nouns = []
	for token in doc :
		tag = f'{token.text} < {token.pos_}'
		if "NOUN" in tag:
			substantif = token
			nouns.append(substantif)
	print("nouns:", nouns)
	return(nouns)

# function to find definition for each noun
def find_definition(nouns):
	# find definition for each noun in nouns
	definitions = []
	for nom in nouns :
		# convert Spacy syntax to string
		nom = nom.text

		if nom not in dictionnaire:

			# define webpage for specific noun
			url = "https://www.larousse.fr/dictionnaires/francais"
			urlpage = url + '/' + nom
			print("urlpage:", urlpage)

			# find webpage
			page = requests.get(urlpage)
			# get content
			soup = BeautifulSoup(page.content, 'lxml')
			# find specific class of definitions
			result = soup.find_all('li', class_="DivisionDefinition")
			# select definition 1
			if result:
				definition = str(result[0])
				#print("definition with tag:", definition)
				if definition:
					# remove example
					example = ': <span class="ExempleDefinition">'
					if example in definition:
						cutdefinition = definition.split(example)
						#print("cutdefinition:", cutdefinition)
						definition = cutdefinition[0] 
					# remove html tag
					clean_definition = re.sub("<(.+?)>", "", definition)
					clean_definition = clean_definition.strip()
					clean = nom, clean_definition
					#print('word + definition:', nom, clean_definition)
				dictionnaire.add(clean) 
	return dictionnaire


# find value of key in dictionary
dictionnaire = set()
for branche in arbres.values():
	citation = branche

	nouns = find_nouns(citation)
	print("nouns:", nouns)
	if nouns:
		dictionnaire = find_definition(nouns)

print('dictionnaire first round:', dictionnaire)
print('length dictionnaire def:', len(dictionnaire))

# save dictionnaire as pickle
dumping_data = open('definitions1.obj', 'wb') 
pickle.dump(dictionnaire, dumping_data)


