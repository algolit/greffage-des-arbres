Greffage des arbres
par Algolit, avec l’aide de Alice Neron

Imaginez les livres comme des arbres transformés. Certains ne vivent qu'un an, d'autres dix ans, certains restent en vie pendant des siècles. À quoi ressemblerait la greffe d'un de ces arbres ?

Greffer des arbres reprend des citations sur les arbres tirées d'ouvrages existants. Selon les circonstances, ces greffons ont une croissance rapide ou lente. Dans cette œuvre, chaque greffe est défini par son jardinier, qui est présent avec une courte biographie grattée sur Wikipédia. Le greffon pousse pendant des saisons différentes. La croissance est indiquée en couleur. Elle dépend des définitions des substantifs trouvées dans la version en ligne de Larousse.

Ce script utilise Python et les librairies Spacy, Wikipedia, Colored.


Original idea:
This script automatises the Oulipo constraint Litterature Definitionelle:
http://oulipo.net/fr/contraintes/litterature-definitionnelle
invented by Marcel Benabou in 1966
In a given phrase, one replaces every significant element (noun, adjective, verb, adverb) 
by one of its definitions in a given dictionary ; one reiterates the operation on the newly received phrase, 
and again.

Copyright (C) 2020 Constant, Algolit, Anaïs Berck, Gijs de Heij, Alice Neron

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details: <http://www.gnu.org/licenses/>.
